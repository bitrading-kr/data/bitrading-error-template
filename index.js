const version = "0.1.1";
const CommonStatus = [
  {
    name: 'CONN',
    message: '연결 에러',
    child: [ // Reason
      {
        name: 'REFUSE',
        message: '연결 거절'
      },
      {
        name: 'TIMEOUT',
        message: '연결 시간 지연'
      },
      {
        name: 'DB',
        message: 'DB 연결 실패'
      }
    ]
  },
  {
    name: 'PARAM',
    message: '파라미터 에러',
    child: [
      {
        name: 'INVALID',
        message: '적합하지 않음'
      }
    ]
  },
  {
    name: 'EXEC',
    message: '실행 에러'
  }
];

const CommonReason = [
  {
    name: 'UNKNOWN',
    message: '알 수 없음'
  }
];

const ErrorList = [
  {
    name: 'UNKNOWN',
    message: '알 수 없는 에러'
  },
  {
    name: 'AUTH',
    message: '인증',
    child: [ // result
      {
        name: 'AUTH',
        message: '인증 실패',
        child: [ // status
          {
            name: 'ACCESSKEY',
            message: 'Access Key 문제',
            child: [ // reason
              {
                name: 'EXPIRED',
                message: '만료됨'
              }
            ]
          },
          {
            name: 'TOKEN',
            message: '토큰 문제',
            child: [
              {
                name: 'EXPIRED',
                message: '만료됨'
              }
            ]
          }
        ]
      },
      {
        name: 'LOGIN',
        message: '로그인 실패',
        child: [
          {
            name: 'FAILED',
            message: '실패',
            child: [
              {
                name: 'NOMATCH',
                message: '아이디 혹은 비밀번호가 일치하지 않습니다.'
              }
            ]
          }
        ]
      },
      {
        name: ''
      }
    ]
  },
  {
    name: 'Resource',
    message: '리소스'
  },
  {
    name: 'User',
    message: '사용자'
  },
  {
    name: 'Group',
    message: '그룹'
  },
  {
    nmae: 'Order',
    message: '오더'
  },
  {
    name: 'Trading',
    message: '거래'
  }
];

module.exports = { ErrorList, CommonStatus, CommonReason, version };
